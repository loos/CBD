#!/bin/bash

mkdir $1

cd $1
mkdir 6-31+G_d  AVDZ  AVQZ  AVTZ
cd 6-31+G_d

echo -e '$comment' > CBD_sf_td_$1_6_31G_d.inp
echo -e "SF-$1" >> CBD_sf_td_$1_6_31G_d.inp
echo -e '$end\n' >> CBD_sf_td_$1_6_31G_d.inp

echo '$molecule' >> CBD_sf_td_$1_6_31G_d.inp
echo '0 3' >> CBD_sf_td_$1_6_31G_d.inp
echo 'C     0.000000    1.017702    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo "C     1.017702   -0.000000    0.000000" >> CBD_sf_td_$1_6_31G_d.inp
echo 'C    -1.017702    0.000000    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo 'C    -0.000000   -1.017702    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo 'H     0.000000    2.092429    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo 'H     2.092429   -0.000000    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo 'H    -0.000000   -2.092429    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo 'H    -2.092429    0.000000    0.000000' >> CBD_sf_td_$1_6_31G_d.inp
echo -e '$end' >> CBD_sf_td_$1_6_31G_d.inp

echo '$rem' >> CBD_sf_td_$1_6_31G_d.inp
echo 'JOBTYPE = sp' >> CBD_sf_td_$1_6_31G_d.inp
echo "METHOD = $1" >> CBD_sf_td_$1_6_31G_d.inp
echo 'BASIS = 6-31+G*' >> CBD_sf_td_$1_6_31G_d.inp
echo 'PURECART = 1111' >> CBD_sf_td_$1_6_31G_d.inp
echo 'SCF_CONVERGENCE = 9' >> CBD_sf_td_$1_6_31G_d.inp
echo 'THRESH = 12' >> CBD_sf_td_$1_6_31G_d.inp
echo 'MAX_SCF_CYCLES = 100' >> CBD_sf_td_$1_6_31G_d.inp
echo 'MAX_CIS_CYCLES = 100' >> CBD_sf_td_$1_6_31G_d.inp
echo 'SPIN_FLIP = TRUE' >> CBD_sf_td_$1_6_31G_d.inp
echo 'UNRESTRICTED = TRUE' >> CBD_sf_td_$1_6_31G_d.inp
echo 'CIS_N_ROOTS = 8' >> CBD_sf_td_$1_6_31G_d.inp
echo 'CIS_SINGLETS = TRUE' >> CBD_sf_td_$1_6_31G_d.inp
echo 'CIS_TRIPLETS = TRUE' >> CBD_sf_td_$1_6_31G_d.inp
echo 'RPA = FALSE' >> CBD_sf_td_$1_6_31G_d.inp
echo '$end' >> CBD_sf_td_$1_6_31G_d.inp

echo -e '#!/bin/bash' > q_chem
echo -e "#SBATCH --job-name=SF-$1" >> q_chem
echo -e '#SBATCH --nodes=1' >> q_chem
echo -e '#SBATCH -n 4' >> q_chem
echo -e '#SBATCH -p q-chem' >> q_chem
echo -e '#SBATCH --mem=20000' >> q_chem

echo -e "qchem CBD_sf_td_$1_6_31G_d.inp CBD_sf_td_$1_6_31G_d.log" >> q_chem

sbatch q_chem

cd ..
cd AVDZ

echo -e '$comment' > CBD_sf_td_$1_avdz.inp
echo -e "SF-$1" >> CBD_sf_td_$1_avdz.inp
echo -e '$end\n' >> CBD_sf_td_$1_avdz.inp

echo '$molecule' >> CBD_sf_td_$1_avdz.inp
echo '0 3' >> CBD_sf_td_$1_avdz.inp
echo 'C     0.000000    1.017702    0.000000' >> CBD_sf_td_$1_avdz.inp
echo "C     1.017702   -0.000000    0.000000" >> CBD_sf_td_$1_avdz.inp
echo 'C    -1.017702    0.000000    0.000000' >> CBD_sf_td_$1_avdz.inp
echo 'C    -0.000000   -1.017702    0.000000' >> CBD_sf_td_$1_avdz.inp
echo 'H     0.000000    2.092429    0.000000' >> CBD_sf_td_$1_avdz.inp
echo 'H     2.092429   -0.000000    0.000000' >> CBD_sf_td_$1_avdz.inp
echo 'H    -0.000000   -2.092429    0.000000' >> CBD_sf_td_$1_avdz.inp
echo 'H    -2.092429    0.000000    0.000000' >> CBD_sf_td_$1_avdz.inp
echo -e '$end' >> CBD_sf_td_$1_avdz.inp

echo '$rem' >> CBD_sf_td_$1_avdz.inp
echo 'JOBTYPE = sp' >> CBD_sf_td_$1_avdz.inp
echo "METHOD = $1" >> CBD_sf_td_$1_avdz.inp
echo 'BASIS = aug-cc-pVDZ' >> CBD_sf_td_$1_avdz.inp
echo 'SCF_CONVERGENCE = 9' >> CBD_sf_td_$1_avdz.inp
echo 'THRESH = 12' >> CBD_sf_td_$1_avdz.inp
echo 'MAX_SCF_CYCLES = 100' >> CBD_sf_td_$1_avdz.inp
echo 'MAX_CIS_CYCLES = 100' >> CBD_sf_td_$1_avdz.inp
echo 'SPIN_FLIP = TRUE' >> CBD_sf_td_$1_avdz.inp
echo 'UNRESTRICTED = TRUE' >> CBD_sf_td_$1_avdz.inp
echo 'CIS_N_ROOTS = 8' >> CBD_sf_td_$1_avdz.inp
echo 'CIS_SINGLETS = TRUE' >> CBD_sf_td_$1_avdz.inp
echo 'CIS_TRIPLETS = TRUE' >> CBD_sf_td_$1_avdz.inp
echo 'RPA = FALSE' >> CBD_sf_td_$1_avdz.inp
echo '$end' >> CBD_sf_td_$1_avdz.inp

echo -e '#!/bin/bash' > q_chem
echo -e "#SBATCH --job-name=SF-$1" >> q_chem
echo -e '#SBATCH --nodes=1' >> q_chem
echo -e '#SBATCH -n 4' >> q_chem
echo -e '#SBATCH -p q-chem' >> q_chem
echo -e '#SBATCH --mem=20000' >> q_chem

echo -e "qchem CBD_sf_td_$1_avdz.inp CBD_sf_td_$1_avdz.log" >> q_chem

sbatch q_chem

cd ..
cd AVTZ

echo -e '$comment' > CBD_sf_td_$1_avtz.inp
echo -e "SF-$1" >> CBD_sf_td_$1_avtz.inp
echo -e '$end\n' >> CBD_sf_td_$1_avtz.inp

echo '$molecule' >> CBD_sf_td_$1_avtz.inp
echo '0 3' >> CBD_sf_td_$1_avtz.inp
echo 'C     0.000000    1.017702    0.000000' >> CBD_sf_td_$1_avtz.inp
echo "C     1.017702   -0.000000    0.000000" >> CBD_sf_td_$1_avtz.inp
echo 'C    -1.017702    0.000000    0.000000' >> CBD_sf_td_$1_avtz.inp
echo 'C    -0.000000   -1.017702    0.000000' >> CBD_sf_td_$1_avtz.inp
echo 'H     0.000000    2.092429    0.000000' >> CBD_sf_td_$1_avtz.inp
echo 'H     2.092429   -0.000000    0.000000' >> CBD_sf_td_$1_avtz.inp
echo 'H    -0.000000   -2.092429    0.000000' >> CBD_sf_td_$1_avtz.inp
echo 'H    -2.092429    0.000000    0.000000' >> CBD_sf_td_$1_avtz.inp
echo -e '$end' >> CBD_sf_td_$1_avtz.inp

echo '$rem' >> CBD_sf_td_$1_avtz.inp
echo 'JOBTYPE = sp' >> CBD_sf_td_$1_avtz.inp
echo "METHOD = $1" >> CBD_sf_td_$1_avtz.inp
echo 'BASIS = aug-cc-pVTZ' >> CBD_sf_td_$1_avtz.inp
echo 'SCF_CONVERGENCE = 9' >> CBD_sf_td_$1_avtz.inp
echo 'THRESH = 12' >> CBD_sf_td_$1_avtz.inp
echo 'MAX_SCF_CYCLES = 100' >> CBD_sf_td_$1_avtz.inp
echo 'MAX_CIS_CYCLES = 100' >> CBD_sf_td_$1_avtz.inp
echo 'SPIN_FLIP = TRUE' >> CBD_sf_td_$1_avtz.inp
echo 'UNRESTRICTED = TRUE' >> CBD_sf_td_$1_avtz.inp
echo 'CIS_N_ROOTS = 8' >> CBD_sf_td_$1_avtz.inp
echo 'CIS_SINGLETS = TRUE' >> CBD_sf_td_$1_avtz.inp
echo 'CIS_TRIPLETS = TRUE' >> CBD_sf_td_$1_avtz.inp
echo 'RPA = FALSE' >> CBD_sf_td_$1_avtz.inp
echo '$end' >> CBD_sf_td_$1_avtz.inp

echo -e '#!/bin/bash' > q_chem
echo -e "#SBATCH --job-name=SF-$1" >> q_chem
echo -e '#SBATCH --nodes=1' >> q_chem
echo -e '#SBATCH -n 4' >> q_chem
echo -e '#SBATCH -p q-chem' >> q_chem
echo -e '#SBATCH --mem=20000' >> q_chem

echo -e "qchem CBD_sf_td_$1_avtz.inp CBD_sf_td_$1_avtz.log" >> q_chem

sbatch q_chem

cd ..
cd AVQZ

echo -e '$comment' > CBD_sf_td_$1_avqz.inp
echo -e "SF-$1" >> CBD_sf_td_$1_avqz.inp
echo -e '$end\n' >> CBD_sf_td_$1_avqz.inp

echo '$molecule' >> CBD_sf_td_$1_avqz.inp
echo '0 3' >> CBD_sf_td_$1_avqz.inp
echo 'C     0.000000    1.017702    0.000000' >> CBD_sf_td_$1_avqz.inp
echo "C     1.017702   -0.000000    0.000000" >> CBD_sf_td_$1_avqz.inp
echo 'C    -1.017702    0.000000    0.000000' >> CBD_sf_td_$1_avqz.inp
echo 'C    -0.000000   -1.017702    0.000000' >> CBD_sf_td_$1_avqz.inp
echo 'H     0.000000    2.092429    0.000000' >> CBD_sf_td_$1_avqz.inp
echo 'H     2.092429   -0.000000    0.000000' >> CBD_sf_td_$1_avqz.inp
echo 'H    -0.000000   -2.092429    0.000000' >> CBD_sf_td_$1_avqz.inp
echo 'H    -2.092429    0.000000    0.000000' >> CBD_sf_td_$1_avqz.inp
echo -e '$end' >> CBD_sf_td_$1_avqz.inp

echo '$rem' >> CBD_sf_td_$1_avqz.inp
echo 'JOBTYPE = sp' >> CBD_sf_td_$1_avqz.inp
echo "METHOD = $1" >> CBD_sf_td_$1_avqz.inp
echo 'BASIS = aug-cc-pVQZ' >> CBD_sf_td_$1_avqz.inp
echo 'SCF_CONVERGENCE = 9' >> CBD_sf_td_$1_avqz.inp
echo 'THRESH = 12' >> CBD_sf_td_$1_avqz.inp
echo 'MAX_SCF_CYCLES = 100' >> CBD_sf_td_$1_avqz.inp
echo 'MAX_CIS_CYCLES = 100' >> CBD_sf_td_$1_avqz.inp
echo 'SPIN_FLIP = TRUE' >> CBD_sf_td_$1_avqz.inp
echo 'UNRESTRICTED = TRUE' >> CBD_sf_td_$1_avqz.inp
echo 'CIS_N_ROOTS = 8' >> CBD_sf_td_$1_avqz.inp
echo 'CIS_SINGLETS = TRUE' >> CBD_sf_td_$1_avqz.inp
echo 'CIS_TRIPLETS = TRUE' >> CBD_sf_td_$1_avqz.inp
echo 'RPA = FALSE' >> CBD_sf_td_$1_avqz.inp
echo '$end' >> CBD_sf_td_$1_avqz.inp

echo -e '#!/bin/bash' > q_chem
echo -e "#SBATCH --job-name=SF-$1" >> q_chem
echo -e '#SBATCH --nodes=1' >> q_chem
echo -e '#SBATCH -n 4' >> q_chem
echo -e '#SBATCH -p q-chem' >> q_chem
echo -e '#SBATCH --mem=20000' >> q_chem

echo -e "qchem CBD_sf_td_$1_avqz.inp CBD_sf_td_$1_avqz.log" >> q_chem

sbatch q_chem

cd ..
